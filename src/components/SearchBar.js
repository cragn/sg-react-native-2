import React from 'react';
import { View, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons'

const SearchBar = ({ term, onChangeTerm, onSubmit }) =>
  <View style={styles.bg}>
    <AntDesign name='search1' style={styles.searchIcon} />
    <TextInput
      autoCapitalize='none'
      autoCorrect={false}
      style={styles.searchField}
      value={term}
      onChangeText={onChangeTerm}
      onSubmitEditing={onSubmit}
    />
    { term.length > 0 &&
    <TouchableOpacity onPress={() => onChangeTerm('')} style={styles.clearRegion}>
      <AntDesign name='close' style={styles.clearIcon} />
    </TouchableOpacity>
    }
  </View>;

const styles = StyleSheet.create({
  bg: {
    height: 50,
    backgroundColor: '#ddd',
    margin: 25,
    borderRadius: 10,
    flexDirection: 'row',
  },
  searchIcon: {
    fontSize: 24,
    alignSelf: 'center',
    marginLeft: 10
  },
  searchField: {
    flex: 1,
    fontSize: 18,
    paddingHorizontal: 10
  },
  clearRegion: {
    alignSelf: 'center',
    padding: 15
  },
  clearIcon: {
    fontSize: 10,
    color: '#999'
  }
});

export default SearchBar;