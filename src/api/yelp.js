import axios from 'axios';

const token = '58ZnZa0zuxI1kXH5mhM6c7NAaf-5BPAV3t9tW-aesWP-vuA-K9eTIunM0wlwc0wyN_mBRx54KeC7UMLQyMnYNQC8uFqAbr-QkKcjUiAK355uOWdDB-A-cqDThrDvXXYx';

export default axios.create({
  baseURL: 'https://api.yelp.com/v3/businesses',
  headers: {
    Authorization: `Bearer ${token}`
  }
});