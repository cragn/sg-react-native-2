import React, { useState } from 'react';
import { View, Text } from 'react-native';
import SearchBar from '../components/SearchBar';
import yelp from '../api/yelp';

const SearchScreen = () => {
  const [term, setTerm] = useState('');
  const [results, setResults] = useState([]);
  const [errorMsg, setErrorMsg] = useState('');

  const search = async () => {
    try {
      const response = await yelp.get('/search', {
        params: {
          term,
          limit: 50,
          location: 'Wroclaw, Poland'
        }
      });
      const { businesses } = response.data;
      setResults(businesses);
    } catch (err) {
      setErrorMsg('Something went wrong.');
    }
  }

  return (
    <View>
      <SearchBar
        term={term}
        onChangeTerm={setTerm}
        onSubmit={search}
      />
      <Text>You are looking for: {term}</Text>
      <Text>Found: {results.length} results</Text>
      { errorMsg ? <Text>{errorMsg}</Text> : null}
    </View>
  );
};

export default SearchScreen;